# webandcloud

This is an essentials web programming session

1. using html 
2. tags
3. collecting input
4. consuming API POST and GET
5. using css
6. using javascript

# note

this code - especially the part about consuming JSON API service works in tandem with this API service - http://simplewebapi1webapp1.azurewebsites.net/

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 
