
function hello_there()
{
    var functionname = "hello_there"; 
    var cavemanstringenter = "entering " + functionname;
    var cavemanstringleave = "leaving " + functionname;
    console.log(cavemanstringenter);

    document.getElementById("dom_1").innerHTML = "welcome to js training";

    var something1 = "Exciting times lie ahead of us";
    var something2 = 10;
    var something3 = 4.5;
    var finalmessage = " something 1 " + something1 + " something 2 " + something2 + "something 3 " + something3;
    console.log(finalmessage);

    console.log(cavemanstringleave);
}

function apicallhpget()
{
    var functionname = "apicallhpget"; 
    var cavemanstringenter = "entering " + functionname;
    var cavemanstringleave = "leaving " + functionname;
    console.log(cavemanstringenter);

    //collect the value from the alert box
    var name = document.getElementById("name").value;

    var actualoutput = "Hello " + name + ". Today all your wishes will come true.";

    var finalmessage = actualoutput;
    console.log(finalmessage);

    var trollingoutput = "I'm sorry, " + name + ". I'm afraid I can't do that. Nor can I open the pod bay doors."

    document.getElementById("dom_1").innerHTML = trollingoutput;

    //lets do our get here. 
    var baseurl = "http://simplewebapi1webapp1.azurewebsites.net/";
    var endpoint = "api/Headphones/";
    var id = 7;
    var fullurl = baseurl+endpoint+id;
    console.log("full url is "+fullurl);

    //doing the actual get
    //this does not work, check function definition for actual details.
    //Get(fullurl);
    //trying another technique
    Get2(fullurl);

    console.log(cavemanstringleave);
}

//this is for making a simple post request
function apicallhppost()
{
    var functionname = "apicallhppost"; 
    var cavemanstringenter = "entering " + functionname;
    var cavemanstringleave = "leaving " + functionname;
    console.log(cavemanstringenter);

    //Okay get is done. 
    //lets do a POST. 
    //lets do our get here. 
    var baseurl = "http://simplewebapi1webapp1.azurewebsites.net/";
    var endpoint = "api/Headphones/";
    var fullurlpost = baseurl+endpoint;
    console.log("full url is "+fullurlpost);    
    Post1(fullurlpost);


    console.log(cavemanstringleave);
}

//this wont work becuase of two reasons
//Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental 
//effects to the end user’s experience. For more help http://xhr.spec.whatwg.org/
//Cross-Origin Request Blocked: The Same Origin Policy disallows reading the remote 
//resource at http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/7. 
//(Reason: CORS header ‘Access-Control-Allow-Origin’ missing).
function Get(yourUrl)
{

    var functionname = "Get"; 
    var cavemanstringenter = "entering " + functionname;
    var cavemanstringleave = "leaving " + functionname;
    console.log(cavemanstringenter);

    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    var responsefromAPI =  Httpreq.responseText;  
    
    var finalmessage = responsefromAPI;
    console.log(finalmessage);
    
    console.log(cavemanstringleave);
}

//trying another solution. 

function Get2(yourUrl)
{
    var functionname = "Get2"; 
    var cavemanstringenter = "entering " + functionname;
    var cavemanstringleave = "leaving " + functionname;
    console.log(cavemanstringenter);

    //this is the actual function that does the get call.
    var getJSON = function(url, callback) 
    {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onload = function() {
          var status = xhr.status;
          if (status === 200) {
            callback(null, xhr.response);
          } else {
            callback(status, xhr.response);
          }
        };
        xhr.send();
    };

    getJSON(yourUrl,
    function(err, data) 
    {
        if (err !== null) 
        {
            alert('Something went wrong: ' + err);
        }
        else 
        {
            console.log(data);
            var text = "Headphone Details - Category - "+ data.Category + 
                        " ID " + data.Id +
                        " Name - " + data.Name +
                        " Price - " + data.Price + 
                         " Warehouse -  " + data.WarehouseName;
            console.log(text);
            document.getElementById("headphoneoutput").innerHTML = text;
        }
    });

    console.log(cavemanstringleave);
}


function Post1(yourUrl)
{
    var functionname = "Post1"; 
    var cavemanstringenter = "entering " + functionname;
    var cavemanstringleave = "leaving " + functionname;
    console.log(cavemanstringenter);

    //build our object.
    var headphoneforpostobject = {
        "Id": 1,
        "Name": "pavan headphone1",
        "Category": "sample string 3",
        "Price": 4.0,
        "WarehouseId": 10,
        "Warehouse": {
          "Id": 10,
          "Warehouse_Name": "pavan warehouse5"
        }
      }
    var headphoneforpostobjectasjsonstring = JSON.stringify(headphoneforpostobject);
    var jsonpostdata = headphoneforpostobjectasjsonstring;

    //this is the actual function that does the post call.
    var getJSON = function(url, callback) 
    {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.responseType = 'json';
        xhr.setRequestHeader("Content-type", "application/json");
        //"Access-Control-Allow-Origin":"*"
        xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
        xhr.onload = function() {
          var status = xhr.status;
          if (status === 200) {
            callback(null, xhr.response);
          } else {
            callback(status, xhr.response);
          }
        };
        xhr.send(jsonpostdata);
    };

    getJSON(yourUrl,
    function(err, data) 
    {
        if (err !== null) 
        {
            if(err == 201)
            {
                console.log(data);
                var text = "Headphone Details - " + 
                            " ID " + data.Id +
                            " Name - " + data.Name +
                             " Warehouse -  " + data.WarehouseName;
                console.log(text);
                document.getElementById("headphoneoutputpost").innerHTML = text;                
            }
            else
            {
                alert('Something went wrong: ' + err);
            }

        }
        else 
        {
            console.log(data);
            var text = "Headphone Details - " + 
                        " ID " + data.Id +
                        " Name - " + data.Name +
                         " Warehouse -  " + data.WarehouseName;
            console.log(text);
            document.getElementById("headphoneoutputpost").innerHTML = text;
        }
    });

    console.log(cavemanstringleave);
}


