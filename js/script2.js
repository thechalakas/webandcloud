
function hello_there()
{
    var functionname = "hello_there"; 
    var cavemanstringenter = "entering " + functionname;
    var cavemanstringleave = "leaving " + functionname;
    console.log(cavemanstringenter);

    document.getElementById("dom_1").innerHTML = "welcome to js training";

    var something1 = "Exciting times lie ahead of us";
    var something2 = 10;
    var something3 = 4.5;
    var finalmessage = " something 1 " + something1 + " something 2 " + something2 + "something 3 " + something3;
    console.log(finalmessage);

    console.log(cavemanstringleave);
}

function hello_there2()
{
    var functionname = "hello_there2"; 
    var cavemanstringenter = "entering " + functionname;
    var cavemanstringleave = "leaving " + functionname;
    console.log(cavemanstringenter);

    //collect the value from the alert box
    var name = document.getElementById("name").value;

    var actualoutput = "Hello " + name + ". Today all your wishes will come true.";

    var trollingoutput = "I'm sorry, " + name + ". I'm afraid I can't do that. Nor can I open the pod bay doors."

    document.getElementById("dom_1").innerHTML = trollingoutput;

    var finalmessage = actualoutput;
    console.log(finalmessage);

    console.log(cavemanstringleave);
}
